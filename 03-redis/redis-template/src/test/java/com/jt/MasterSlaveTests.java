package com.jt;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

@SpringBootTest
public class MasterSlaveTests {
    @Autowired
    private RedisTemplate redisTemplate;
    @Test
    void testMasterReadWrite(){//redis6379 主节点可以读写
        ValueOperations vo = redisTemplate.opsForValue();
        vo.set("ip", "172.17.0.2");
        vo.set("role", "master");
        Object role = vo.get("role");
        System.out.println(role);
    }
    @Test
    void testSlaveRead(){//redis6380 从节点只支持读
        ValueOperations vo = redisTemplate.opsForValue();
        Object role = vo.get("role");
        System.out.println(role);
    }
}
