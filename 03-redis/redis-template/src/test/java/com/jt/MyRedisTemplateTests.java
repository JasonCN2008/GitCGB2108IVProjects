package com.jt;

import com.jt.redis.pojo.Blog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.util.List;
import java.util.Map;
import java.util.Set;

@SpringBootTest
public class MyRedisTemplateTests {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 将blog对象以json串的方式写到redis数据库，
     * 并将其读出了进行输出
     */
    @Test
    void testBlogJson(){//课堂练习
        //1.构建Blog对象
        Blog blog=new Blog();
        blog.setId(100);
        blog.setTitle("hello redis");
        //2.将Blog对象以json方式写入到redis
        ValueOperations valueOperations =
                redisTemplate.opsForValue();
        valueOperations.set("blog", blog);
        //3.读取数据并进行输出
        blog=(Blog)valueOperations.get("blog");
        System.out.println(blog);
    }


}
