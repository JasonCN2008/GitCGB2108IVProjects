package com.jt;

import com.jt.redis.pojo.Menu;
import com.jt.redis.service.MenuService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MenuServiceTests {
    @Autowired
    @Qualifier(value="defaultMenuService")
    private MenuService menuService;

    @Test
    void testUpdateMenu(){
        Menu menu = menuService.selectById(4L);
        menu.setPermission("sys:resource:insert");
        menuService.updateMenu(menu);
    }

    @Test
    void testInsertMenu(){
      Menu menu=new Menu();
      menu.setName("daochu resource");
      menu.setPermission("sys:resource:daochu");
      menuService.insertMenu(menu);
    }

    @Test
    void testSelectById(){
        Menu menu = menuService.selectById(5L);
        System.out.println(menu);
    }

}
