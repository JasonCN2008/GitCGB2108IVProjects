package com.jt.redis.service;

import com.jt.redis.pojo.Menu;

public interface MenuService {
    Menu selectById(Long id);
    Menu insertMenu(Menu menu);
    Menu updateMenu(Menu menu);
}
