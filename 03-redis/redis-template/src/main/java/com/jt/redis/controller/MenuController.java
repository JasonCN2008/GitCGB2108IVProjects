package com.jt.redis.controller;

import com.jt.redis.pojo.Menu;
import com.jt.redis.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    @Qualifier("defaultMenuService")
    private MenuService menuService;
    @GetMapping("/{id}")
    public Menu doSelectById(@PathVariable("id") Long id){
        return menuService.selectById(id);
    }
    @PostMapping
    public String doInsertMenu(@RequestBody Menu menu){
        menuService.insertMenu(menu);
        return "insert ok";
    }
    @PutMapping
    public String doUpdateMenu(@RequestBody Menu menu){
        menuService.updateMenu(menu);
        return "update ok";
    }
}
