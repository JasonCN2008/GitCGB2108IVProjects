package com.jt;

import com.google.gson.Gson;
import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class JedisTests {
    /**
     * 以hash类型方式，存储一个map结构的数据。
     * 具体如何实现？自己尝试(课堂练习)
     */
    @Test
    public void testStringHash01(){
        //1.建立连接
        Jedis jedis=new Jedis("192.168.192.128", 6379);//TCP/IP
        //2.hash类型数据操作
        //2.1构建key
        String token= UUID.randomUUID().toString();
        //2.2基于key存储字段名(小key)，字段值
//        jedis.hset(token, "id", "100");
//        jedis.hset(token, "name", "tony");
        //2.3直接基于hash类型存储一个map对象
        Map<String,String> map=new HashMap<>();
        map.put("id","100");
        map.put("name", "jack");
        jedis.hset(token, map);
        jedis.expire(token, 10);//设置key的有效期
        jedis.hset(token, "name", "Mike");
        Map<String, String> userMap = jedis.hgetAll(token);
        System.out.println(userMap);
        //3.释放资源
        jedis.close();
    }
    /**
     * 将对象转换为json格式字符串，
     * 然后存储到redis。
     */
    @Test
    public void testStringOper2(){
        //1.建立连接
        Jedis jedis=new Jedis("192.168.192.128", 6379);
        //2.将对象转换为json字符串然后存储到redis
        Map<String,String> userMap=new HashMap<>();//将来可以是pojo
        userMap.put("id","100");
        userMap.put("name", "jack");
        Gson gson=new Gson();//Google提供的操作json数据的api
        String userJson=gson.toJson(userMap);//将对象转换为json格式字符串
        String token= UUID.randomUUID().toString();
        System.out.println("token="+token);
        jedis.set(token, userJson);
        jedis.expire(token, 10);//设置key的有效期为10秒中
        userJson=jedis.get(token);
        System.out.println("userJson="+userJson);
        userMap=gson.fromJson(userJson, Map.class);//将json字符串转换为map
        System.out.println(userMap);
        //3.释放资源
         jedis.close();
    }
    /**
     * 测试字符串的操作
     */
    @Test
    public void testStringOper1(){
        //1.建立连接
        Jedis jedis=new Jedis("192.168.192.128", 6379);
        //2.执行字符串操作
        //2.1添加(insert)
        jedis.set("id", "100");
        jedis.set("name", "tony");
        //2.2更新(update)
        jedis.incr("id");
        jedis.incrBy("id", 10);
        jedis.set("name", "Mike");
        //2.3查询(select)
        String id = jedis.get("id");
        String name=jedis.get("name");
        jedis.append("log", "set operation");
        String log=jedis.get("log");
        System.out.printf("id=%s,name=%s,log=%s \n",id,name,log);//%s表示字符串占位符
        //2.4)删除(delete)
        Long result = jedis.del("name");
        System.out.printf("result=%d",result);//%d表示数字占位符
        //3.释放资源
        jedis.close();
    }

    /**测试是否可以连接到远端的redis*/
    @Test
    public void testGetConnection(){
       //1.建立连接(jedis对象为操作远端redis的核心api)
        Jedis jedis=new Jedis("192.168.192.128", 6379);
        //jedis.auth("123456"); 假如远端redis必须输入密码，则执行这个操作
       //2.执行ping操作
        String ping = jedis.ping();//PONG
        System.out.println(ping);
    }
}
